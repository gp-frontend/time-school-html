/*----------------------------------------
 VALIDATIONS JQUERY FORM
 ----------------------------------------*/
$(function () {

  /* RULES */

  $('#form-feedback').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-feedback-message').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-review').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-recording').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-thanks').gpPopup();
    }
  });

  $('#form-vacancies').validate({
    focusCleanup: true,
    focusInvalid: false,
    errorElement: 'span',
    errorClass: 'error',
    messages: {
      required: "Это поле обязательное",
    },
    submitHandler: function (element) {
      $('#popup-thanks').gpPopup();
    }
  });

})
